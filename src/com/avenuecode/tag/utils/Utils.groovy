package com.avenuecode.tag.utils


/**
 * Reads .version file from current workspace
 * @return
 */
String readVersionFile(script, path=''){
    def filePath = '.version'
    if(path != ''){
        filePath = "$path/.version"
    }
    def content = script.readFile(encoding: 'utf-8', file: filePath)
    return content.trim()
}

/**
 * Returns the next version tag by incrementing
 * the current tag by one. If current is empty
 * return the first applicable tag using pattern
 * @param current
 * @return
 */
def getNextTag(String current){
    def last = current[-1]
    def separator = current.lastIndexOf('.')

    // current == 1.0: last == 0
    // current == 1: separator == -1
    if(last == '0' || separator == -1){ // Both cases we need to start incrementing by 1.
        return "$current.1"
    }
    // Here we know that we will increment by the last number
    // current == 1.0.2: minor == 2, rest == 1.0, return: 1.0.3
    def minor = current.substring(separator + 1, current.length())
    def rest = current.substring(0, separator)
    // Try to cast it to integer
    def next = minor.toInteger() + 1
    return "$rest.$next"
}

/**
 * Execute git command to list repository tags.
 * If pattern provided list only tags that
 * starts with given pattern and that are not
 * equals to the pattern
 *
 * E.g: pattern=1.0 git tag -l = [1.0, 1.0.1, 2.0, 2.1.1]
 * Would return [1.0.1]
 * @param pattern
 * @return
 */
def listTags(script, String pattern=''){
    def tagList = script.sh(returnStdout: true, script: 'git tag -l')
    if (tagList != ''){
        // Loop tags array. Returning only tags that starts
        // with given pattern
        return tagList.split('\n').findAll { index->
            index.startsWith(pattern) && index != pattern
        }
    }
    return [] // No tags =/
}

/**
 * Removes protocol from url
 * @param url
 */
def removeProtocolFromUrl(String url){
    return url.replace("https://","")
            .replace("http://","")
}

/**
 * Publish git tag
 * @param script Script class
 * @param tag Git tag
 * @param repositoryURL Full https url
 * @return
 */
def pushTag(script, tag, repositoryURL){
    def repository = removeProtocolFromUrl(repositoryURL)

    script.withCredentials([[$class: 'UsernamePasswordMultiBinding',
                             credentialsId: 'ac-gitlab',
                             usernameVariable: 'GIT_USERNAME',
                             passwordVariable: 'GIT_PASSWORD']]) {
        def baseURL = "https://${GIT_USERNAME}:${GIT_PASSWORD}"
        def repositoryCommiterEmail = 'ac-jenkins@avenuecode.com'
        def repositoryCommiterUsername = 'ac-jenkins'

        script.sh("git fetch --prune ${baseURL}@${repository} '+refs/tags/*:refs/tags/*'")
        script.sh("git tag ${tag}")
        script.sh("git push ${baseURL}@${repository} --tags")
    }
}