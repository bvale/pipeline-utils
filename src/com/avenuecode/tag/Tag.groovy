package com.avenuecode.tag

import com.avenuecode.tag.utils.Utils


class Tag implements Serializable{

    private List<String> tags
    private String last
    private String next
    private String pattern

    def utils
    def scm

    /**
     * Holds Jenkins pipeline steps class so we can use it inside
     * this package. When testing we can pass a Stub script or
     * use test.Script which provides a valid implementation
     * of the most used Jenkins methods
     */
    def script

    Tag(script) {
        this.utils = new Utils()
        this.script = script
        this.pattern = utils.readVersionFile(script)
    }

    Tag(script, scm) {
        this.utils = new Utils()
        this.script = script
        this.scm = scm
    }

    String getPattern() {
        if(!pattern){
            this.pattern = utils.readVersionFile(script)
        }
        return pattern
    }
/**
     * Return all tags that starts with .version file content
     * @return
     */
    def getTags(){
        if(!tags){
            tags = utils.listTags(script, getPattern())
        }
        return tags
    }

    /**
     * Last created tag.
     * @return
     */
    def getLast(){
        // Last tag should consider .version and not the g tag -l
        if(!last){
            // We need to explicitly call getTags() here
            // other wise the method is not executed.
            last = (getTags()) ? tags[-1] : ''
        }
        return last
    }

    /**
     * Name of the next tag to be created
     * @return
     */
    def getNext(){
        if(!next){
            // We need to explicitly call getTags() here
            // other wise the method is not executed.
            // Check if we have tags, if not return the first applicable from
            // pattern
            next = (getTags()) ? utils.getNextTag(getLast()) : "$pattern.1"
        }
        return next
    }

    /**
     * Push next tag to git repository
     */
    void push(){
        utils.pushTag(script, next, scm.GIT_URL)
        tags = null // Scan again
        next = null
        last = null
    }
}
