package com.avenuecode.kubernetes

import com.avenuecode.kubernetes.utils.Utils

class Pipeline implements Serializable{

    private String app
    private String color
    private String next
    private String commit
    private String namespace
    private String environment
    private String domain // avenuecode.com ? avenuecode.io

    def utils
    /**
     * Holds Jenkins pipeline steps class so we can use it inside
     * this package. When testing we can pass a Stub script or
     * use test.Script which provides a valid implementation
     * of the most used Jenkins methods
     */
    def script

    Pipeline(String app) {
        this.app = app
        this.namespace = 'default'
        this.domain = 'avenuecode.com'
        this.environment = 'staging'
        this.utils = new Utils()
    }

    Pipeline(String app, script) {
        this.app = app
        this.script = script
        this.namespace = 'default'
        this.domain = 'avenuecode.com'
        this.environment = 'staging'
        this.utils = new Utils()
    }

    String getDomain() {
        return domain
    }

    void setDomain(String domain) {
        this.domain = domain
    }

    String getNamespace() {
        return namespace
    }

    void setNamespace(String namespace) {
        this.namespace = namespace
    }

    @Deprecated
    String getCommit() {
        if(!commit){
            this.commit = this.utils.commit
        }
        return commit
    }


    String getApp(){
        return app
    }

    String getColor() {
        if(!color) {
            def service = utils.getIngressService(app, namespace, "${app}.${domain}")
            def name = service.serviceName
            if (name) {
                color = name.split('-')[2] // app-namespace-color
            }
        }
        return color
    }

    String getNext() {
        if(!next){
            next = (color == 'green') ? 'blue' : 'green'
        }
        return next
    }

    def switchOver(){
        this.utils.switchOver(app, environment, domain, namespace, next)
        color = next
        next = null
    }
}


