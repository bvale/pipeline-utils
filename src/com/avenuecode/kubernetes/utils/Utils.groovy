package com.avenuecode.kubernetes.utils

import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic


@Deprecated
String getCommit() {
    return sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
}

@Deprecated
String getBranch() {
    return sh(returnStdout: true, script: 'git rev-parse --abbrev-ref HEAD').trim()
}

/**
 * Marshaling Map to JSON
 * @param data
 * @return
 */

def marshallJSON(Map data){
    // WARNING: We might need @NonCPS here
    return JsonOutput.toJson(data)
}


def jsonParser(String data){
    // WARNING: We might need @NonCPS here
    def jsonSlurper = new JsonSlurperClassic()
    return jsonSlurper.parseText(data)
}

/**
 * Collects the JSON ingress policy of given
 * application that is inside given kubernetes
 * namespace
 * @param app Application identifier
 * @param namespace Kubernetes namespace
 * @return JSON ingress policy
 */
def getIngress(String app, String namespace){
    def cmd = "kubectl -n ${namespace} get -o json ingress/${app}"
    def output = script.sh(returnStdout: true, script: cmd)
    def ingress = jsonParser(output)
    return ingress
}

def getIngressService(String app, String namespace, String serviceName){
    def ingress = getIngress(app, namespace)

    def rules = ingress.spec.rules
    if(rules){
        def serviceDefinition = rules.find{ rule->
            rule.host == serviceName
        }
        return serviceDefinition.http.paths[0].backend
    }
}



def switchOver(String app, String environment, String domain, String namespace, String next){

    def ingress = getIngress(app, namespace)

    def rules = ingress.spec.rules
    if(rules) {
        def serviceDefinition = rules.find { rule ->
            rule.host == "${app}.${domain}"
        }
        serviceDefinition.http.paths[0].backend.serviceName = "$app-$environment-$next"
    }
    println "Starting switch"
    def json = marshallJSON(ingress)

    println "Switching application ${app} to ${next}"


    // We need to create a file using `writeFile`. If we use groovy methods it will be written at master
    writeFile file: "tmp_ingressUpdate.json", text: json
    sh "kubectl apply -f ${WORKSPACE}/tmp_ingressUpdate.json"
}
