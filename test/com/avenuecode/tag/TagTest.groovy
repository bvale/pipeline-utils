package com.avenuecode.tag

import com.avenuecode.tag.utils.TagStub
/**
 * Created by bvale on 21/07/2017.
 */
class TagTest extends GroovyTestCase{


    void testGetLast(){
        def tag = new Tag(new TagStub('', '1.0'))
        assertEquals('', tag.last)

        tag = new Tag(new TagStub('1.0.1\n1.0.2', '1.0'))
        assertEquals('1.0.2', tag.last)

        tag = new Tag(new TagStub('1.1', '1.1'))
        assertEquals('', tag.last)
    }

    void testGetNext(){
        def tag = new Tag(new TagStub('', '1.0'))
        assertEquals('1.0.1', tag.next)

        tag = new Tag(new TagStub('1.0.1\n1.0.2', '1.0'))
        assertEquals('1.0.3', tag.next)

        tag = new Tag(new TagStub('1.1', '1.1'))
        assertEquals('1.1.1', tag.next)

    }
}
