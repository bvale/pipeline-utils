package com.avenuecode.tag.utils

import com.avenuecode.tag.Script
/**
 * Created by bernardovale on 7/29/17.
 */
class UtilsTest extends GroovyTestCase{

    def script

    @Override
    protected void setUp() throws Exception {
        super.setUp()
        // Our test version of Jenkins functions
        this.script = new Script()
    }

    void testGetNextTag(){
        def utils = new Utils()

        // Test Suite
        def suite = [
                [current: '1.0.1', expected: '1.0.2'],
                [current: '1.0', expected: '1.0.1'],
                [current: '1', expected: '1.1']
        ]

        suite.each{ test->
            def got = utils.getNextTag(test.current)
            assertEquals(test.expected, got)
        }
    }

    void testListTags(){
        def utils = new Utils()

        // Test Suite
        def suite = [
                [   content: '1.0.1\n1.0.2',
                    pattern: '1.0',
                    expected: ['1.0.1', '1.0.2']
                ],
                [   content: '',
                    pattern: '1.0',
                    expected: []
                ],
                [   content: '1.0\n2.0\n3.0',
                    pattern: '',
                    expected: ['1.0', '2.0', '3.0']
                ],
                [   content: '1.0\n2.3.1\nfoo\n1.0.1\n1.0.2\nv4.1',
                    pattern: '1.0',
                    expected: ['1.0.1', '1.0.2']
                ],
                [
                        content: '1.1',
                        pattern: '1.1',
                        expected: []
                ]
        ]
        suite.each{ test->
            def got = utils.listTags(new TagStub(test.content), test.pattern)
            assertEquals(test.expected, got)
        }
    }

    void testRemoveProtocolFromUrl(){
        def utils = new Utils()
        def got = utils.removeProtocolFromUrl("https://gitlab.com/bvale/todo-app.git")
        assertEquals("gitlab.com/bvale/todo-app.git", got)

        got = utils.removeProtocolFromUrl("http://gitlab.com/bvale/todo-app.git")
        assertEquals("gitlab.com/bvale/todo-app.git", got)
    }
}