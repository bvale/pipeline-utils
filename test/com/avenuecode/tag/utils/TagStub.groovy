package com.avenuecode.tag.utils

/**
 * Created by bernardovale on 7/29/17.
 */
class TagStub {

    def scriptContent
    def fileContent

    TagStub(String scriptContent){
        this.scriptContent = scriptContent
    }

    TagStub(scriptContent, fileContent) {
        this.scriptContent = scriptContent
        this.fileContent = fileContent
    }

    def sh(Map args = [:]){
            return this.scriptContent
        }

    def readFile(Map args = [:]){
        return this.fileContent
    }
}
