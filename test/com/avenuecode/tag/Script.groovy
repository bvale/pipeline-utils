package com.avenuecode.tag

/**
 * This class implements functions that we have inside Jenkins.
 * To make tests easier we're implementing similar functions
 * and call them only when testing.
 */
class Script {

    def sh(Map args = [:]){
        def content = args.script.execute().text
        if (args.returnStdout){
            return content
        }
    }

    def readFile(Map args = [:]){
        String currentDir = new File('.').getAbsolutePath()
        def file = new File(currentDir, args.file).getCanonicalFile()
        try{
            def lines = file.readLines()
            return lines.join('\n')
        }catch (ignored){
            return ""
        }
    }
}
